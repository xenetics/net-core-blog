var path = require('path')
var webpack = require('webpack')

module.exports = {
  "entry": {
    main: './wwwroot/vueapp/main.js',
    newPost: './wwwroot/vueapp/newPost.js'
  },
  "output": {
    "path": path.resolve(__dirname, './wwwroot/vueapp/dist'),
    "publicPath": '/wwwroot/vueapp/dist/',
    "filename": "[name].js"
  },
  "module": {
    "rules": [{
      "test": /\.vue$/,
      "loader": "vue-loader"
    }, {
      "test": /\.js$/,
      "loader": "babel-loader",
      "exclude": /node_modules/
    }, {
      test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
      loader: 'url',
      query: {
        limit: 10000
        // ,
        // name: utils.assetsPath('img/[name].[hash:7].[ext]')
      }
    }]
  },
  "plugins": [],
  "resolve": {
    "alias": {
      "vue$": "vue/dist/vue"
    }
  }
}
