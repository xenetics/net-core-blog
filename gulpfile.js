var gulp = require('gulp');
var webpack = require('gulp-webpack');

gulp.task('webpack', function() {
  return gulp.src('wwwroot/vueapp/main.js')
    .pipe(webpack());
});
