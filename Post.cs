using System;

namespace DbTest.Models {
  public class Post {

    public int PostId { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public int? PosterId { get; set; }
    public DateTime? Created { get; set; }
    public DateTime? Modified { get; set; }

  }
}
