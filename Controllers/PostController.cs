using System;
using Microsoft.AspNetCore.Mvc;
using DbTest.Models;
using System.Linq;
using System.Collections.Generic;

namespace DbTest.Controllers {
  public class PostController : Controller {

    private IPostRepository repository;

    public PostController(IPostRepository repo) {
      repository = repo;
    }

    [HttpPost]
    public Post Update(Post post) {
      Console.WriteLine(post.PostId);
      repository.AddPost(post);
      return post;
    }

    [HttpGet]
    public Post Get(int id) {
      return repository.Posts.Where(p => p.PostId == id).FirstOrDefault();
    }

    [HttpGet]
    public ViewResult New() {
      return View();
    }
  }
}
