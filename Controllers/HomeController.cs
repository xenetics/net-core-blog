using System;
using Microsoft.AspNetCore.Mvc;
using DbTest.Models;
using System.Linq;
using System.Collections.Generic;

namespace DbTest.Controllers {
  public class HomeController : Controller {

    IPostRepository repository;

    public HomeController(IPostRepository repo) {
      this.repository = repo;
    }

    public ViewResult Home() {
      return View();
    }

    public IEnumerable<Post> PostIndex() {
      return repository.Posts;
    }

    public string Test() {
      return "Hi, this is a test.";
    }
  }
}
