import Vue          from 'vue'
import NewPostPage  from './NewPostPage.vue'

new Vue({
  el: '.new.post',
  render: h => h(NewPostPage)
})
