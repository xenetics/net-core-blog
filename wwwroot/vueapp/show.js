import Vue          from 'vue'
import ShowPostPage from './components/ShowPostPage.vue'

new Vue({
  el: '.show.post',
  render: h => h(ShowPostPage)
})
