using System.Collections.Generic;

namespace DbTest.Models {
  public interface IPostRepository {
    IEnumerable<Post> Posts { get; }

    Post AddPost(Post post);
  }
}
