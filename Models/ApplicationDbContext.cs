using Microsoft.EntityFrameworkCore;
using System;

namespace DbTest.Models {
  public class ApplicationDbContext : DbContext {

    public ApplicationDbContext() {}

    protected override void OnConfiguring(DbContextOptionsBuilder builder) {
      builder.UseSqlite("Filename=./Blog.db");
    }

    public DbSet<Post> Posts { get; set; }
  }
}
