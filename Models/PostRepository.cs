using System.Collections.Generic;
using System;
using System.ComponentModel;
using System.Linq;

namespace DbTest.Models {
  public class PostRepository : IPostRepository {
    private ApplicationDbContext context = new ApplicationDbContext();

    public IEnumerable<Post> Posts => context.Posts;

    public Post AddPost(Post post) {
      if (post.PostId == 0) {
        post.Created = DateTime.Now;

        context.Posts.Add(post);

        context.SaveChanges();
      } else {
        Post existingPost = context.Posts.Where(p => p.PostId == post.PostId).FirstOrDefault();

        existingPost.Modified = DateTime.Now;
        existingPost.Content = post.Content;
        existingPost.Title = post.Title;

        context.SaveChanges();
      }
      return post;
    }
  }
}

// foreach(PropertyDescriptor descriptor in TypeDescriptor.GetProperties(post))
// {
//   string name=descriptor.Name;
//   object value=descriptor.GetValue(post);
//   Console.WriteLine("{0}={1}",name,value);
// }
